<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Index Route
Route::get('/', function () { return redirect()->route('dashboard.index'); })->name('index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    // Authenticate routes
    Route::group(['prefix' => 'auth'], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('login');
        Route::post('login', 'AuthController@login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });

    // Application routes
    Route::group(['middleware' => 'auth'], function () {
        // Dashboard (home page)
        Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
        // Resource Routes
        Route::resource('publishers', 'PublisherController', ['except' => ['edit']]);
        Route::resource('players', 'PlayerController', ['except' => ['show']]);
        Route::resource('sessions', 'SessionController', ['except' => ['show']]);
        Route::resource('users', 'UserController', ['except' => ['show']]);
    });
});

Route::get('player/{name}', 'MediaController@getEmbeddedPlayer')->name('media.player');
Route::post('media/callback/publish', 'MediaController@onPublish')->name('media.callback.publish');
