<?php

namespace App\Providers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\ServiceProvider;

class ResponseMarcoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $alert = function ($level) {
            return (function ($message, $title = null) use ($level) {
                $keys = ['level', 'message'];
                if (isset($title)) {
                    array_push($keys, 'title');
                }

                return $this->withAlert(compact($keys));
            })->bindTo($this);
        };

        RedirectResponse::macro('alert', function ($level, $message, $title = null) use ($alert) {
            return $alert->call($this, $level)($message, $title);
        });
        RedirectResponse::macro('info', $alert('info'));
        RedirectResponse::macro('success', $alert('success'));
        RedirectResponse::macro('warning', $alert('warning'));
        RedirectResponse::macro('danger', $alert('danger'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
