<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Morph to publisher.
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function publishable()
    {
        return $this->morphOne(Publisher::class, 'source');
    }
}
