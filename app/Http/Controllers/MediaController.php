<?php

namespace App\Http\Controllers;

use App\Car;
use App\Desktop;
use App\Publisher;
use App\Services\VideoJS\VideoJS;
use App\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MediaController extends Controller
{
    /**
     * Logger for media publishing.
     * @var Logger
     */
    private $logger;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        // Prepare logger
        $this->logger = new Logger('media-publishing');
        $this->logger->pushHandler(
            new StreamHandler(storage_path('logs/media-'.Carbon::now()->format('Y-m-d').'.log'))
        );
    }

    public function getEmbeddedPlayer(Request $request, $name)
    {
        $videojs = VideoJS::make([
            'name' => 'meranote-media-player',
            'url' => 'https://media-ivan.meranote.in.th/media/hls/' . $name . '.m3u8'
        ])->build();

        return view('components.videojs.embedded', compact('videojs'));
    }

    /**
     * On Publish Callback.
     * Called internally by nginx-rtmp when got publish,
     * this endpoint will authorize the publish requests,
     * determine media, make session, and response back to
     * nginx-rtmp for continue processing.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function onPublish(Request $request)
    {
        $this->logger->debug('Receive publish request.');
        $this->logger->debug(var_export($request->all(), true));

        // Validate input
        $this->logger->debug('Validating Input.');
        $validator = Validator::make($request->all(), [
            'token' => 'required|string|max:255',
            'secret' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            $this->logger->debug('Input is invalid, Unauthorized.');
            return response(null, Response::HTTP_BAD_REQUEST);
        }
        $this->logger->debug('Input is valid, continue check token and secret.');

        // Find the association of token, secret and stream_name
        // If none is found then we reject the publishing (as Unauthorized)
        /** @var Publisher $publisher */
        $publisher = Publisher::where('token', $request->get('token'))
            ->where('secret', $request->get('secret'))
            ->first();
        if (isset($publisher)) {
            $this->logger->debug('Token and secret matched, check stream name (target).');
            $target = $publisher->getTarget();
            // If stream name is match the target of the token, secret
            // Start processing on publish request
            if ($request->get('name') === $target) {
                $this->logger->info('Stream name match, Authorized for published.');
                switch ($request->get('call')) {
                    case 'publish': return $this->onStartPublish($request, $publisher);
                    case 'publish_done': return $this->onEndPublish($request, $publisher);
                    // Unknown call action, bypass
                    default: return response(null, Response::HTTP_NO_CONTENT);
                }
            }
        } else {
            $this->logger->debug('Token and secret not matched, Unauthorized.');
        }

        return response(null, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when publisher start live streaming.
     *
     * @param Request $request
     * @param Publisher $publisher
     * @return \Illuminate\Http\Response
     */
    protected function onStartPublish(Request $request, Publisher $publisher)
    {
        // If type is not live, reject
        if ($request->get('type') !== 'live') {
            $this->logger->debug('Publish type not live, reject.');
            return response(null, Response::HTTP_BAD_REQUEST);
        }

        $this->logger->info('Start Publishing stream='.$request->get('name').'.');
        // Make and associate live session to publisher
        $publisher->live_session()->associate(Session::create([
            'name' => $publisher->getTarget().Carbon::now()->format('Y-m-d_his'),
            'publisher_id' => $publisher->id,
            'key' => $this->generatekey(),
            'start_at' => Carbon::now(),
        ]));
        $publisher->save();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Called when publisher complete live streaming.
     *
     * @param Request $request
     * @param Publisher $publisher
     * @return \Illuminate\Http\Response
     */
    protected function onEndPublish(Request $request, Publisher $publisher)
    {
        $this->logger->info('Finish Publishing stream='.$request->get('name').'.');
        // De-associate live session of publisher
        $publisher->live_session()->associate(null);
        $publisher->save();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    protected function generatekey($length = 32)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz!@#$^_";
        $result = '';

        for ($ignored = 0; $ignored < $length; $ignored++) {
            $rNum = rand(0, strlen($chars));
            $result .= substr($chars, $rNum, 1);
        }

        return $result;
    }
}
