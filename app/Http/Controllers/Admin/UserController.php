<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * List all users.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->eloquent(User::query())
                ->addColumn('action', function (User $user) {
                    return '<a href="'.route('users.edit', ['user' => $user->id]).'" class="btn btn-primary">Edit</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        $table = datatables()->getHtmlBuilder()->columns([
            ['data' => 'id', 'title' => '#'],
            ['data' => 'email', 'title' => 'Username'],
            ['data' => 'name', 'title' => 'Name'],
            ['data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]
        ]);

        return view('users.index', compact('table'));
    }

    /**
     * Store new user.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate input
        $this->validate($request, [
            'email' => 'required|string|max:255|unique:users,email',
            'name' => 'required|string|max:255',
            'password' => 'required|string|max:255'
        ]);

        // Create User
        User::create([
            'email' => $request->get('email'),
            'name' => $request->Get('name'),
            'password' => bcrypt($request->get('password')),
        ]);

        return redirect()
            ->back()
            ->success(__('users.created'));
    }

    /**
     * Show form for edit user.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update user.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function update(Request $request, User $user)
    {
        $changed = false;
        switch ($request->get('target')) {
            case 'detail': {
                $this->validate($request, [
                    'email' => 'required|string|max:255',
                    'name' => 'required|string|max:255',
                ]);
                if (count(array_diff($user->only(['email', 'name']), $request->only(['email', 'name']))) > 0) {
                    $user->name = $request->get('name');
                    $user->email = $request->get('email');
                    $changed = true;
                }
            } break;
            case 'password': {
                $this->validate($request, [
                    'password' => 'required|string|max:255',
                ]);
                $user->password = bcrypt($request->get('password'));
                $changed = true;
            }
        }

        if ($changed) {
            $user->save();
            return redirect()
                ->back()
                ->success(__('users.updated', ['id' => $user->id]));
        } else {
            return redirect()
                ->back()
                ->warning(__('form.no-updated', ['id' => $user->id]));
        }
    }

    /**
     * Remove user.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // TODO: Implement
    }
}
