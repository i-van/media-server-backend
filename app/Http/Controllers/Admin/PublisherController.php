<?php

namespace App\Http\Controllers\Admin;

use App\Car;
use App\Desktop;
use App\Http\Controllers\Controller;
use App\Publisher;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return datatables()
                ->eloquent(Publisher::query()->with('source'))
                ->editColumn('source_type', function (Publisher $publisher) {
                    $source = $publisher->source;
                    if ($source instanceof Car) {
                        return 'Car';
                    } else {
                        return 'Desktop';
                    }
                })
                ->addColumn('target', function (Publisher $publisher) {
                    return $publisher->getTarget();
                })
                ->addColumn('live', function (Publisher $publisher) {
                    if (is_null($publisher->live_session)) {
                        return '<span class="label label-live label-default">no</span>';
                    } else {
                        return '<a href="'.route('sessions.edit', ['session' => $publisher->live_session->id]).'"><span class="label label-danger">live</span></a>';
                    }
                })
                ->addColumn('action', function (Publisher $publisher) {
                    return '<button class="btn btn-primary btn-view-modal" onclick="openViewModal('.$publisher->id.')">Show Details</a>';
                })
                ->rawColumns(['live', 'action'])
                ->toJson();
        }

        $table = datatables()->getHtmlBuilder()->columns([
            ['data' => 'id', 'title' => '#'],
            ['data' => 'source_type', 'title' => 'Type'],
            ['data' => 'target', 'title' => 'Target', 'sortable' => false, 'orderable' => false],
            ['data' => 'token', 'title' => 'Token'],
            ['data' => 'live', 'title' => 'Live Status'],
            ['data' => 'created_at', 'title' => 'Created At'],
            ['data' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'action', 'title' => 'Action', 'orderable' => false, 'searchable' => false]
        ]);

        return view('publishers.index', compact('table'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate input
        $this->validate($request, [
            'type' => 'required|string|max:255|in:Car,Desktop',
            'target' => 'required|string|max:255',
        ]);

        // Create Publisher
        $publisher = new Publisher([
            'token' => $this->generateToken(),
            'secret' => $this->generateToken(),
        ]);
        // Associate source type
        $associateModel = null;
        switch ($request->get('type')) {
            case 'Car': {
                $associateModel = Car::create([
                    'car_id' => $request->get('target'),
                ]);
            } break;
            case 'Desktop': {
                $associateModel = Desktop::create([
                    'name' => $request->get('target'),
                ]);
            } break;
        }
        $publisher->source()->associate($associateModel);
        $publisher->save();

        return redirect()
            ->back()
            ->success(__('publishers.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param Publisher $publisher
     * @return \Illuminate\Http\Response
     */
    public function show(Publisher $publisher)
    {
        return view('publishers.view-fragment', compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function generateToken($length = 32)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz!@#$^_";
        $result = '';

        for ($ignored = 0; $ignored < $length; $ignored++) {
            $rNum = rand(0, strlen($chars));
            $result .= substr($chars, $rNum, 1);
        }

        return $result;
    }
}
