<?php

namespace App\Services\VideoJS;

class VideoJSPlayerBuilder
{
    /**
     * @var VideoJSPlayerConfigurator
     */
    private $configurator;

    public function __construct(VideoJSPlayerConfigurator $configurator)
    {
        $this->configurator = $configurator;
    }

    public function html()
    {
        return view('components.videojs.main', ['videojs' => $this->configurator->getConfiguration()]);
    }

    public function script()
    {
        return view('components.videojs.script', ['videojs' => $this->configurator->getConfiguration()]);
    }
}