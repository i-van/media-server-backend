<?php

namespace App\Services\VideoJS;


class VideoJS
{
    private function __construct() {}

    public static function make($config = []): VideoJSPlayerConfigurator
    {
        return new VideoJSPlayerConfigurator($config);
    }
}