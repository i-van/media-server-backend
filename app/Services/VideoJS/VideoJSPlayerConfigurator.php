<?php

namespace App\Services\VideoJS;

class VideoJSPlayerConfigurator
{
    /**
     * Configuration bag.
     * @var array
     */
    protected $config;

    public function __construct($config = [])
    {
        $this->config = array_merge([
            'name' => 'videojs-player',
            'url' => 'http://localhost/video.m3u8',
            'settings' => config('videojs.settings'),
        ], $config);
    }

    public function getConfiguration(): array
    {
        return $this->config;
    }

    public function clearPlayerSize(): self
    {
        $this->config['settings']['width'] = null;
        $this->config['settings']['height'] = null;
        return $this;
    }

    public function setPlayerSize($width, $height): self
    {
        $this->config['settings']['width'] = $width;
        $this->config['settings']['height'] = $height;
        return $this;
    }

    public function setName($url): self
    {
        $this->config['name'] = $url;
        return $this;
    }

    public function setUrl($url): self
    {
        $this->config['url'] = $url;
        return $this;
    }

    /**
     * Build an configuration for player.
     * @return VideoJSPlayerBuilder
     */
    public function build(): VideoJSPlayerBuilder
    {
        return new VideoJSPlayerBuilder($this);
    }
}