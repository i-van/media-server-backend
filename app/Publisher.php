<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'secret'
    ];

    /**
     * Morph To (reverse).
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function source()
    {
        return $this->morphTo();
    }

    /**
     * Related to all sessions.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * Related to current session.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function live_session()
    {
        return $this->belongsTo(Session::class, 'current_session', 'id');
    }

    /**
     * Related to all players of this publisher.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function players()
    {
       return $this->hasMany(Player::class);
    }

    /**
     * Get target of this publisher (source's target)
     */
    public function getTarget()
    {
        switch ($this->source_type) {
            case Car::class: return $this->source->car_id;
            case Desktop::class: return $this->source->name;
            default: return null;
        }
    }
}
