<?php

return [

    'title' => [
        'info' => 'ข้อมูล',
        'success' => 'สำเร็จ',
        'warning' => 'คำเตือน',
        'danger' => 'เกิดข้อผิดพลาด',
    ]

];
