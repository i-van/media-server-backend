<form class="form-horizontal" onsubmit="((e) => e.preventDefault())()">

    <div class="box-body">
        <div class="form-group">
            <label for="v_id" class="col-sm-2 control-label">Id</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="v_id" id="v_id" value="{{ $publisher->id }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="v_secret" class="col-sm-2 control-label">Type</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="v_secret" id="v_secret" value="{{ $publisher->secret }}" readonly>
            </div>
        </div>

        @if($publisher->source instanceof \App\Car)
            <div class="form-group">
                <label for="v_target" class="col-sm-2 control-label">Car ID</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" name="v_target" id="v_target" value="{{ $publisher->source->car_id }}" readonly>
                </div>
            </div>
        @elseif($publisher->source instanceof \App\Desktop)
            <div class="form-group">
                <label for="v_target" class="col-sm-2 control-label">Desktop Name</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" name="v_target" id="v_target" value="{{ $publisher->source->name }}" readonly>
                </div>
            </div>
        @endif

        <div class="form-group">
            <label for="v_token" class="col-sm-2 control-label">Token</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="v_token" id="v_token" value="{{ $publisher->token }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="v_secret" class="col-sm-2 control-label">Secret</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="v_secret" id="v_secret" value="{{ $publisher->secret }}" readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="secret" class="col-sm-2 control-label">Is Currently Live</label>

            <div class="col-sm-10">
                @if (is_null($publisher->live_session))
                    <span class="label label-live-form label-default">no</span>
                @else
                    <a href="{{ route('sessions.edit', ['session' => $publisher->live_session->id]) }}"
                       class="btn btn-danger btn-live">live</a>
                @endif
            </div>
        </div>
    </div>

</form>
<hr />
<h3>To start live streaming</h3>
<form class="form-horizontal" onsubmit="((e) => e.preventDefault())()">
    <div class="box-body">
        <div class="form-group">
            <label for="rtmp_root" class="col-sm-2 control-label">RTMP Root</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="rtmp_root" value="rtmp://35.197.157.192/surveillance" readonly>
            </div>
        </div>

        <div class="form-group" style="margin-bottom: 0px;">
            <label for="stream_name" class="col-sm-2 control-label">Stream Name</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="stream_name" value="{{ $publisher->getTarget() }}" readonly>
            </div>
        </div>

        <h4 class="col-sm-12 control-label" style="margin-bottom: 1.85rem; text-align: center; font-weight: bold;">Parameters</h4>

        <div class="form-group">
            <label for="p_token" class="col-sm-2 control-label">token</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="p_token" value="{{ $publisher->token }}" readonly>
            </div>
        </div>

        <div class="form-group" style="margin-bottom: 0px;">
            <label for="p_secret" class="col-sm-2 control-label">secret</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="p_secret" value="{{ $publisher->secret }}" readonly>
            </div>
        </div>
    </div>
</form>
<h5>Video will available at: <a href="{{ route('media.player', ['name' => $publisher->getTarget()]) }}" target="_blank">{{ route('media.player', ['name' => $publisher->getTarget()]) }}</a></h5>
<hr />
<h3>Streaming Configuration Presets</h3>
<hr />
<h4><b>OBS</b></h4>
<p>Set stream type to <code>Custom Streaming Server</code> and set the configurations as below.</p>
<form onsubmit="((e) => e.preventDefault())()">
    <div class="form-group">
        <label for="obs_url" class="control-label">URL</label>
        <input type="text" class="form-control" id="obs_url" value="rtmp://35.197.157.192/surveillance" readonly />
    </div>
    <div class="form-group">
        <label for="obs_stream_key" class="control-label">Steam key</label>
        <textarea class="form-control" id="obs_stream_key" readonly>{{ $publisher->getTarget() }}?token={{ $publisher->token }}&secret={{ $publisher->secret }}</textarea>
    </div>
</form>
<hr />
<h4><b>FFmpeg</b></h4>
<p>Copy these path for live streaming.</p>
<form onsubmit="((e) => e.preventDefault())()">
    <div class="form-group">
        <label for="obs_stream_key" class="control-label">Full Path</label>
        <textarea class="form-control" id="obs_stream_key" readonly>rtmp://35.197.157.192/surveillance/{{ $publisher->getTarget() }}?token={{ $publisher->token }}&secret={{ $publisher->secret }}</textarea>
    </div>
</form>
