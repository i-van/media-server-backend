<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Publisher</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('publishers.store') }}" method="post" class="form-horizontal" autocomplete="off">
                @csrf

                <input type="hidden" autocomplete="false">

                <div class="box-body">
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Type</label>

                        <div class="col-sm-10">
                            <select class="form-control" name="type" id="type">
                                <option value="" selected disabled>Please select type</option>
                                <option value="Car">Car</option>
                                <option value="Desktop">Desktop</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="target" id="target-label" class="col-sm-2 control-label">Target</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="target" id="target" required disabled>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Add Publisher</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
