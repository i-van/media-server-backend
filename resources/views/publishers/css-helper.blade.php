<style>
    .label-live {
        font-size: 1.25rem;
    }

    .btn-live, .label-live-form {
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 1.75rem;
        font-weight: bold;
        line-height: 1.228571;
    }
</style>