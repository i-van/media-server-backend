<script>
    $('#type').change(function () {
        let label = $('#target-label');
        let target = $('#target');
        switch ($(this).val()) {
            case 'Car': {
                label.html('Car ID');
            } break;
            case 'Desktop': {
                label.html('Desktop Name');
            } break;
        }
        target.attr('disabled', false);
        target.val('');
    });
</script>