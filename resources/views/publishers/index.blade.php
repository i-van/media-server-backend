@extends('layouts.main')

@section('styles')
    @include('vendors.datatables.css')
    @include('publishers.css-helper')
    <style>
        textarea {
            resize: none;
        }
    </style>
@endsection

@section('header')
    <h1>
        Publishers
    </h1>
@endsection

@section('body')

    @include('publishers.create-fragment')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Publisher list</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! $table->table() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="modal-view">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Publisher Details: <span id="modal-view-id"></span></h4>
                </div>
                <div class="modal-body" id="modal-view-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('scripts')
    @include('vendors.datatables.js')
    @include('components.js-helper')
    @include('publishers.js-helper')
    {!! $table->scripts() !!}

    <script type="text/javascript">
        function openViewModal(id) {
            let buttons = $('.btn-view-modal');
            buttons.html('Loading...');
            buttons.attr('disabled', true);

            $.get(`{{ url('admin/publishers/') }}/${id}`)
                .done(function(data) {
                    $('#modal-view-id').html(id);
                    $('#modal-view-body').html(data);
                    $('#modal-view').modal('toggle');
                })
                .always(function() {
                    buttons.html('Show Details');
                    buttons.attr('disabled', false);
                });
        }
    </script>
@endsection
