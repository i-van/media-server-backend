@extends('layouts.app')

@section('body-class', 'skin-'.config('app.color').' sidebar-mini')

@section('content')
<div class="wrapper">
    @include('components.topbar')
    @include('components.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <!-- Main content -->
        <section class="content">
            @include('components.alert')

            @yield('body')
        </section>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> WIP
        </div>
        <strong>iVan media server - backend</strong>
    </footer>
</div>
@endsection