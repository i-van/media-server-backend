@if(isset($errors) && count($errors->all()) > 0)
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>{{ __('error.title.danger') }}</h4>
                {{ $errors->first() }}
            </div>
        </div>
    </div>
@elseif($alert = session('alert'))
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-{{ $alert['level'] }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>{{ isset($alert['title']) ? $alert['title'] : __('error.title.'.$alert['level']) }}</h4>
                {{ $alert['message'] }}
            </div>
        </div>
    </div>
@endif
