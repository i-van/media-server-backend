<aside class="main-sidebar">

    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">

            <li class="header">NAVIGATION</li>
            <li class="{{ isActiveRoute('publishers.*') }}">
                <a href="{{ route('publishers.index') }}">
                    <i class="fa fa-video-camera"></i> <span>Publisher Management</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('players.*') }}">
                <a href="{{ route('players.index') }}">
                    <i class="fa fa-tv"></i> <span>Player Management</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('sessions.*') }}">
                <a href="{{ route('sessions.index') }}">
                    <i class="fa fa-play-circle"></i> <span>Session Management</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('users.*') }}">
                <a href="{{ route('users.index') }}">
                    <i class="fa fa-users"></i> <span>User Management</span>
                </a>
            </li>

            <li class="header">USER ACTION</li>
            <li>
                <a href="{{ route('logout') }}">
                    <i class="fa fa-circle-o text-red"></i> <span>Logout</span>
                </a>
            </li>

        </ul>
    </section>
<!-- /.sidebar -->
</aside>
