<script type="text/javascript">
    function generateRandomForPassword(elem, length = 12) {
        let chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        let result = '';

        for (let i = 0; i < length; i++) {
            let rNum = Math.floor(Math.random() * chars.length);
            result += chars.substring(rNum, rNum+1);
        }

        elem = $(elem);
        elem.val(result);
        elem.attr('type', 'text');
        elem.attr('readonly', true);
    }

    function generateRandomForToken(elem, length = 32) {
        let chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz!@#$^_";
        let result = '';

        for (let i = 0; i < length; i++) {
            let rNum = Math.floor(Math.random() * chars.length);
            result += chars.substring(rNum, rNum+1);
        }

        elem = $(elem);
        elem.val(result);
        elem.attr('type', 'text');
        elem.attr('readonly', true);
    }
</script>