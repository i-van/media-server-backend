<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>iVan Media Server | Embedded HLS Player</title>

    @include('vendors.videojs.css')
    <style>
        html, body {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        .video-js {
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
    {!! $videojs->html() !!}

    @include('vendors.videojs.js')
    {!! $videojs->script() !!}
</body>
</html>