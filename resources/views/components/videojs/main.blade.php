<video id="{{ $videojs['name'] }}"
       @if(array_has($videojs['settings'], 'width') && $videojs['settings']['width'] !== null) width="{{ $videojs['settings']['width'] }}" @endif
       @if(array_has($videojs['settings'], 'height') && $videojs['settings']['height'] !== null) height="{{ $videojs['settings']['height'] }}" @endif
       class="{{ $videojs['settings']['class'] }}"
       controls>
    <source src="{{ $videojs['url'] }}"
            type="application/x-mpegURL">
</video>