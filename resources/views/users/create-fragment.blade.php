<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add Staff</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('users.store') }}" method="post" class="form-horizontal" autocomplete="off">
                @csrf

                <input type="hidden" autocomplete="false">

                <div class="box-body">
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>

                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" name="password" id="password" maxlength="255" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-flat btn-generate" onclick="generateRandomForPassword('#password')">Generate</button>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Add User</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
