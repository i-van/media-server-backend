@extends('layouts.main')

@section('styles')
    @include('vendors.datatables.css')
@endsection

@section('header')
    <h1>
        Users
    </h1>
@endsection

@section('body')

    @include('users.create-fragment')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">User list</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! $table->table() !!}
                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal modal-danger fade in" id="modal-confirm-delete">
        <form id="modal-confirm-delete-form" method="post" class="form-horizontal">
            @method('DELETE')
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirm Delete Staff</h4>
                    </div>
                    <div class="modal-body">
                        <p id="modal-confirm-delete-message"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline">Delete</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </form>
    </div>--}}
@endsection

@section('scripts')
    @include('vendors.datatables.js')
    @include('components.js-helper')
    {!! $table->scripts() !!}

    {{--<script type="text/javascript">
        function showDeleteModal (authuser_id, authuser_name) {
            let messageTemplate = function (authuser_name) {
                return "Confirm the delete action of user '" + authuser_name + "' ?";
            };
            let linkTemplate = function (authuser_id) {
                return "{{ url('users') }}/" + authuser_id;
            };

            $("#modal-confirm-delete-form").attr(
                'action',
                linkTemplate(authuser_id)
            );
            $("#modal-confirm-delete-message").html(
                messageTemplate(authuser_name)
            );
            $("#modal-confirm-delete").modal("toggle");
        };
    </script>--}}
@endsection
