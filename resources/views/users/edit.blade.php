@extends('layouts.main')

@section('header')
    <h1>
        Edit User: <b>{{ $user->Name }}</b>
    </h1>
@endsection

@section('body')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">User Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('users.update', ['user' => $user->id ]) }}" method="post" class="form-horizontal">
                @method('PATCH')
                @csrf
                
                <div class="box-body">
                    <div class="form-group">
                        <label for="id" class="col-sm-2 control-label">id</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="id" id="id" value="{{ $user->id }}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-success pull-right" name="target" value="detail">Update</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Update password</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('users.update', ['user' => $user->id ]) }}" method="post" class="form-horizontal">
                @method('PATCH')
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>

                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" class="form-control" name="password" id="password" maxlength="255" required>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-flat btn-generate" onclick="generateRandomForPassword('#password')">Generate</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
                    <button type="submit" class="btn btn-success pull-right" name="target" value="password">Update</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection

@section('scripts')
    @include('components.js-helper')
@endsection
