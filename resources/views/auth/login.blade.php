@extends('layouts.app')

@section('body-class', 'login-page')

@section('content')
<div class="wrapper">
    <div class="login-box">

        <div class="login-logo">
            <b>{{ env('APP_NAME') }}</b>
        </div>
        <!-- /.login-logo -->

    <div class="login-box-body">
        <p class="login-box-msg">Sign in</p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('login') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="email" placeholder="Email">
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="fa fa-key form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
</div>
@endsection