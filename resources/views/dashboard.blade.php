@extends('layouts.main')

@section('header')
    <h1>
        Dashboard
        <small>Admin tool</small>
    </h1>
@endsection

@section('body')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Hello World</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    This is dashboard.
                </div>
            </div>
        </div>
    </div>
@endsection