<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Video JS Configuration file
    |--------------------------------------------------------------------------
    |
    | This file contain settings for videojs videojs.
    |
    */

    'settings' => [
        'class' => 'video-js vjs-default-skin'
    ]

];