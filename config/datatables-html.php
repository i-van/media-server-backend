<?php

return [
    /**
     * Default table attributes when generating the table.
     */
    'table' => [
        'class' => 'table table-striped table-hover table-bordered',
        'id'    => 'dataTableBuilder',
    ],
];
